require('dotenv').config();

module.exports = {
  env: {
    APP_ENV: process.env.APP_ENV
  },
};
